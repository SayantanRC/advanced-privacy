/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.trackers.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import org.koin.java.KoinJavaComponent.get

class TrackersService : Service() {
    companion object {
        const val ACTION_START = "foundation.e.privacymodules.trackers.intent.action.START"

        var coroutineScope = CoroutineScope(Dispatchers.IO)
    }

    override fun onBind(intent: Intent): IBinder? {
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (ACTION_START == intent?.action) {
            stop()
            start()
        }
        return START_REDELIVER_INTENT
    }

    private fun start() {
        coroutineScope = CoroutineScope(Dispatchers.IO)
        get<DNSBlocker>(DNSBlocker::class.java).apply {
            filterHostnameUseCase.writeLogJob(coroutineScope)
            listenJob(coroutineScope)
        }
    }

    private fun stop() {
        kotlin.runCatching { coroutineScope.cancel() }
    }
}
