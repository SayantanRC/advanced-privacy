/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.trackers.service

import android.content.Context
import android.content.Intent
import foundation.e.advancedprivacy.domain.entities.FeatureState
import foundation.e.advancedprivacy.domain.usecases.VpnSupervisorUseCase
import foundation.e.advancedprivacy.trackers.domain.externalinterfaces.TrackersSupervisor
import foundation.e.advancedprivacy.trackers.service.TrackersService.Companion.ACTION_START
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.isActive
import org.koin.core.module.dsl.bind
import org.koin.core.module.dsl.factoryOf
import org.koin.core.module.dsl.singleOf
import org.koin.core.qualifier.named
import org.koin.dsl.module

class TrackersSupervisorEos(private val context: Context) : TrackersSupervisor {

    override val state: StateFlow<FeatureState> = MutableStateFlow(FeatureState.ON)

    override fun start(): Boolean {
        val intent = Intent(context, TrackersService::class.java)
        intent.action = ACTION_START
        return context.startService(intent) != null
    }

    override fun stop(): Boolean {
        return context.stopService(Intent(context, TrackersService::class.java))
    }

    override fun isRunning(): Boolean {
        return TrackersService.coroutineScope.isActive
    }

    override val dnsFilterForIpScrambling = null
}

val trackerServiceModule = module {
    factoryOf(::DNSBlocker)
    singleOf(::TrackersSupervisorEos) {
        bind<TrackersSupervisor>()
    }
    single<VpnSupervisorUseCase> {
        VpnSupervisorUseCaseEos(
            localStateRepository = get(),
            orbotSupervisor = get(),
            trackersSupervisor = get(),
            appDesc = get(named("AdvancedPrivacy")),
            permissionsPrivacyModule = get(),
            scope = get()
        )
    }
}
