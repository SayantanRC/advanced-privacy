/*
 * Copyright (C) 2022 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.privacymodules.fakelocationdemo

import android.Manifest
import android.app.AppOpsManager
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.Process.myUid
import android.util.Log
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import foundation.e.advancedprivacy.domain.entities.AppOpModes
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.entities.ProfileType
import foundation.e.advancedprivacy.fakelocation.domain.usecases.FakeLocationModule
import foundation.e.advancedprivacy.permissions.externalinterfaces.PermissionsPrivacyModuleImpl
import foundation.e.privacymodules.fakelocationdemo.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    companion object {
        const val TAG = "fakeLoc"
    }

    private val fakeLocationModule: FakeLocationModule by lazy { FakeLocationModule(this) }
    private val permissionsModule by lazy { PermissionsPrivacyModuleImpl(this) }

    private lateinit var binding: ActivityMainBinding

    private val appDesc by lazy {
        ApplicationDescription(
            packageName = packageName,
            uid = myUid(),
            label = getString(R.string.app_name),
            icon = null,
            profileId = 0,
            profileType = ProfileType.MAIN
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        actionBar?.setDisplayHomeAsUpEnabled(true)

        binding.view = this
    }

    override fun onResume() {
        super.onResume()
        updateData("")
    }

    private fun updateData(mockedLocation: String) {
        binding.granted = permissionsModule.getAppOpMode(appDesc, AppOpsManager.OPSTR_MOCK_LOCATION) == AppOpModes.ALLOWED

        binding.mockedLocation = mockedLocation
    }

    private val listener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            binding.currentLocation = "lat: ${location.latitude} - lon: ${location.longitude}"
        }

        @Deprecated("Deprecated since API 29, never called.")
        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            TODO("Not yet implemented")
        }

        override fun onProviderEnabled(provider: String) {
            binding.providerInfo = "onProdivderEnabled: $provider"
        }

        override fun onProviderDisabled(provider: String) {
            binding.providerInfo = "onProdivderDisabled: $provider"
        }
    }

    @Suppress("UNUSED_PARAMETER")
    fun onClickToggleListenLocation(view: View?) {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (binding.toggleListenLocation.isChecked) {
            requireLocationPermissions()
            return
        }

        locationManager.removeUpdates(listener)
        binding.currentLocation = "no listening"
    }

    private fun startLocationUpdates() {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        try {
            Log.d(TAG, "requestLocationUpdates")
            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, // TODO: tight this with fakelocation module.
                1000L,
                1f,
                listener
            )
            binding.currentLocation = "listening started"
        } catch (se: SecurityException) {
            Log.e(TAG, "Missing permission", se)
        }
    }

    private val locationPermissionRequest = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        if (permissions.getOrDefault(Manifest.permission.ACCESS_FINE_LOCATION, false) ||
            permissions.getOrDefault(Manifest.permission.ACCESS_COARSE_LOCATION, false)
        ) {
            startLocationUpdates()
        }
    }

    private fun requireLocationPermissions() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            startLocationUpdates()
        } else {
            // Before you perform the actual permission request, check whether your app
            // already has the permissions, and whether your app needs to show a permission
            // rationale dialog. For more details, see Request permissions.
            locationPermissionRequest.launch(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            )
        }
    }

    @Suppress("UNUSED_PARAMETER")
    fun onClickPermission(view: View?) {
        val isGranted = permissionsModule.setAppOpMode(
            appDesc,
            AppOpsManager.OPSTR_MOCK_LOCATION,
            AppOpModes.ALLOWED
        )

        if (isGranted) {
            updateData("")
            return
        }
        // dev mode disabled
        val alertDialog = AlertDialog.Builder(this)
        alertDialog
            .setTitle("Mock location disabled")
            .setNegativeButton("Cancel") { _, _ ->
            }
        alertDialog.create().show()
    }

    @Suppress("UNUSED_PARAMETER")
    fun onClickReset(view: View?) {
        try {
            fakeLocationModule.stopFakeLocation()
        } catch (e: Exception) {
            Log.e(TAG, "Can't stop FakeLocation", e)
        }
    }

    private fun setFakeLocation(latitude: Double, longitude: Double) {
        try {
            fakeLocationModule.startFakeLocation()
        } catch (e: Exception) {
            Log.e(TAG, "Can't startFakeLocation", e)
        }
        fakeLocationModule.setFakeLocation(latitude, longitude)
        updateData("lat: $latitude - lon: $longitude")
    }

    @Suppress("UNUSED_PARAMETER")
    fun onClickParis(view: View?) {
        setFakeLocation(48.8502282, 2.3542286)
    }

    @Suppress("UNUSED_PARAMETER")
    fun onClickLondon(view: View?) {
        setFakeLocation(51.5287718, -0.2416803)
    }

    @Suppress("UNUSED_PARAMETER")
    fun onClickAmsterdam(view: View?) {
        setFakeLocation(52.3547498, 4.8339211)
    }
}
