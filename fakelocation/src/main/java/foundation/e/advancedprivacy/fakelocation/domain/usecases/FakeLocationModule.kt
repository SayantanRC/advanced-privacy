/*
 * Copyright (C) 2022 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.fakelocation.domain.usecases

import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.location.Location
import android.location.LocationManager
import android.location.LocationManager.GPS_PROVIDER
import android.location.LocationManager.NETWORK_PROVIDER
import android.location.provider.ProviderProperties
import android.os.Build
import android.os.SystemClock
import android.util.Log
import foundation.e.advancedprivacy.fakelocation.services.FakeLocationService

/**
 * Implementation of the functionality of fake location.
 * All of them are available for normal application, so just one version is enough.
 *
 * @param context an Android context, to retrieve system services for example.
 */
class FakeLocationModule(private val context: Context) {
    companion object {
        private const val TAG = "FakeLocationModule"
    }

    /**
     * Handy accessor to the locationManager service.
     * We avoid getting it on module initialization to wait for the context to be ready.
     */
    private val locationManager: LocationManager get() =
        context.getSystemService(LOCATION_SERVICE) as LocationManager

    /**
     * List of all the Location provider that will be mocked.
     */
    private val providers = locationManager.allProviders
        .intersect(listOf(GPS_PROVIDER, NETWORK_PROVIDER))

    /**
     * @see IFakeLocationModule.startFakeLocation
     */
    @Synchronized
    fun startFakeLocation() {
        providers.forEach { provider ->
            try {
                locationManager.removeTestProvider(provider)
            } catch (e: Exception) {
                Log.w(TAG, "Test provider $provider already removed.")
            }

            locationManager.addTestProvider(
                provider,
                false,
                false,
                false,
                false,
                false,
                true,
                true,
                ProviderProperties.POWER_USAGE_LOW,
                ProviderProperties.ACCURACY_FINE
            )
            try {
                locationManager.setTestProviderEnabled(provider, true)
            } catch (e: Exception) {
                Log.e(TAG, "Can't enable test $provider", e)
            }
        }
    }

    fun setFakeLocation(latitude: Double, longitude: Double) {
        context.startService(FakeLocationService.buildFakeLocationIntent(context, latitude, longitude))
    }

    internal fun setTestProviderLocation(latitude: Double, longitude: Double) {
        providers.forEach { provider ->
            val location = Location(provider)
            location.latitude = latitude
            location.longitude = longitude

            // Set default value for all the other required fields.
            location.altitude = 3.0
            location.time = System.currentTimeMillis()
            location.speed = 0.01f
            location.bearing = 1f
            location.accuracy = 3f
            location.elapsedRealtimeNanos = SystemClock.elapsedRealtimeNanos()

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                location.bearingAccuracyDegrees = 0.1f
                location.verticalAccuracyMeters = 0.1f
                location.speedAccuracyMetersPerSecond = 0.01f
            }
            try {
                locationManager.setTestProviderLocation(provider, location)
            } catch (e: Exception) {
                Log.e(TAG, "Can't set location for test provider $provider", e)
            }
        }
    }

    /**
     * @see IFakeLocationModule.stopFakeLocation
     */
    fun stopFakeLocation() {
        context.stopService(FakeLocationService.buildStopIntent(context))
        providers.forEach { provider ->
            try {
                locationManager.setTestProviderEnabled(provider, false)
                locationManager.removeTestProvider(provider)
            } catch (e: Exception) {
                Log.d(TAG, "Test provider $provider already removed.")
            }
        }
    }
}
