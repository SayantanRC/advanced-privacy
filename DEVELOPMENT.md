# AdvancedPrivacy Development Guide
This guide contains development related information to help a developer in getting better understanding of project structure.

## Architecture
The architecture of AdvancedPrivacy is based on [clean architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html). For presentation layer, we use a MVVM design pattern,
with a unique StateFlow<UiState>  for each screen to simplify coherence. Our android app is having single activity multiple fragments, using Android Navigation component.

The project has 2 flavors :
 - eOS: for /e/OS devices, with OS specific signatures, system priviledges and persistent flag
 - standalone: for app's stores, without any priviledges.

Most of the differences between the 2 flavors are separated in distinct modules like :
- permissioneos / permissionstandalone
- trackersserviceseos / trackersservicesstandalone

### Clean Architecture

Clean architecture is the building block of AdvancedPrivacy. It is now a common way of organizing project,
so it helps newcomers. It gives a framework to apply Single Responsibility Principle, having a testable project
and keep files with a decent length.


The detailed file tree:

**Domain**

- entities : data classes, with no logic
- usecases : most of the logic, with no external dependencies

**LocalRepositories**

Repositories of data, that use local datasources, like sqlite database, sharedpreferences or in memory objects.

**NetworkRepositories**

Repositories of data, which fetch and push data to some remote servers.

**InterfaceAdapters**

All the glue to abstract Android framework (and also all it's supported versions) from the domain layer.

- services: Android Services
- broadcastreceivers: Android Broadcast Receivers
- Android Interfaces : abstraction of framework interfaces, like private and priviledges specific API.

**UI**
All the UI related code, with MVVM pattern.

## Good practices

### Use hidden-apis-stubs

Reflexion is avoided. We use an [hidden-apis-stub](./permissionseos/libs/hidden-apis-stub) project to make Android hidden APIs available for development and compilations.
Each stubed API should be documented with link to the source code they stub, for each targeted Android version.

### Use Coroutines and Flow

We use Coroutines and Flow for asynchronous tasks. The previous technologies (RxJava, LiveData) to do that are not welcome :)

### Use Koin for dependency injection

The project use Koin for "DI". It's flexibility is helpful to cleanly separate the flavored modules.


## OrbotService development and maintenance

The IpScrambling relies on a fork of OrbotService, which is a module of Orbot, the Tor proxy for Android. It lays as a submodule to help fork upgrade and patching. More here: [orbotservice/README.md](orbotservice/README.md)

## Code Quality and Style

It use ktlint through spotless. The specific rules are defined in [.editorconfig](./.editorconfig).

To run code style check and formatting tool:
```
./gradlew spotlessCheck
./gradlew spotlessApply
```

If spotlessApply fails, let's discuss to relax the ktllint rules.

## State-of-The-Art

This document present the objectives for the project, some area may still have legacy code organizations.

### Todo/Improvements
- [ ] Add unit tests and code coverage.

# References
1. [Kotlin Flow](https://kotlinlang.org/docs/flow.html)
4. [Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
