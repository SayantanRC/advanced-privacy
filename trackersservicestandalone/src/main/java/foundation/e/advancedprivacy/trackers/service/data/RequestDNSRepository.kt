/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.trackers.service.data

import foundation.e.advancedprivacy.core.utils.runSuspendCatching
import java.net.DatagramPacket
import java.net.DatagramSocket
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.pcap4j.packet.DnsPacket
import timber.log.Timber

class RequestDNSRepository {

    suspend fun processDNS(request: DatagramPacket): DnsPacket? = withContext(Dispatchers.IO) {
        runSuspendCatching {
            var response: DnsPacket? = null
            val datagramSocket = DatagramSocket()
            datagramSocket.send(request)

            // Await response from DNS server
            val buf = ByteArray(1024)
            val packet = DatagramPacket(buf, buf.size)
            datagramSocket.receive(packet)
            val dnsResp = packet.data
            if (dnsResp != null) {
                response = DnsPacket.newPacket(dnsResp, 0, dnsResp.size)
            }
            response
        }.onFailure {
            Timber.w(it, "Can't make DNS request.")
        }.getOrNull()
    }
}
