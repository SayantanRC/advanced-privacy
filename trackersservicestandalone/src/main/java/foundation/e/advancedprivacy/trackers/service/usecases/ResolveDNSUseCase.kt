/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.trackers.service.usecases

import foundation.e.advancedprivacy.core.utils.runSuspendCatching
import foundation.e.advancedprivacy.trackers.domain.usecases.FilterHostnameUseCase
import foundation.e.advancedprivacy.trackers.service.data.NetworkDNSAddressRepository
import foundation.e.advancedprivacy.trackers.service.data.RequestDNSRepository
import java.net.DatagramPacket
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import org.pcap4j.packet.DnsPacket
import org.pcap4j.packet.namednumber.DnsRCode
import timber.log.Timber

@OptIn(DelicateCoroutinesApi::class)
class ResolveDNSUseCase(
    private val networkDNSAddressRepository: NetworkDNSAddressRepository,
    private val filterHostnameUseCase: FilterHostnameUseCase,
    private val requestDNSRepository: RequestDNSRepository,
    private val scope: CoroutineScope = GlobalScope
) {
    private val DNS_PORT = 53

    init {
        filterHostnameUseCase.writeLogJob(scope)
    }

    fun shouldBlock(dnsRequest: DnsPacket?): DnsPacket? {
        if (dnsRequest == null) return null

        return runSuspendCatching {
            val host = dnsRequest.header.questions[0].qName.name
            if (filterHostnameUseCase.shouldBlock(host)) {
                dnsRequest.builder
                    .rCode(DnsRCode.NX_DOMAIN)
                    .response(true).build()
            } else {
                null
            }
        }.onFailure {
            Timber.e(it, "Can't find host")
        }.getOrNull()
    }

    suspend fun processDNS(dnsRequest: DnsPacket): DnsPacket? {
        val blockedDnsResponse = shouldBlock(dnsRequest)

        if (blockedDnsResponse != null) {
            return blockedDnsResponse
        }

        val payload = dnsRequest.rawData
        val packet = DatagramPacket(
            payload,
            payload.size,
            networkDNSAddressRepository.dnsAddress,
            DNS_PORT
        )
        return requestDNSRepository.processDNS(packet)
    }
}
