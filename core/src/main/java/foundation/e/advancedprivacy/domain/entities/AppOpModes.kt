/*
 * Copyright (C) 2022 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.domain.entities

import android.app.AppOpsManager.MODE_ALLOWED
import android.app.AppOpsManager.MODE_DEFAULT
import android.app.AppOpsManager.MODE_ERRORED
import android.app.AppOpsManager.MODE_FOREGROUND
import android.app.AppOpsManager.MODE_IGNORED
import android.os.Build

enum class AppOpModes(val modeValue: Int) {
    ALLOWED(MODE_ALLOWED),
    IGNORED(MODE_IGNORED),
    ERRORED(MODE_ERRORED),
    DEFAULT(MODE_DEFAULT),
    FOREGROUND(if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) MODE_ALLOWED else MODE_FOREGROUND);

    companion object {
        private val byMode = mapOf(
            FOREGROUND.modeValue to FOREGROUND,
            ALLOWED.modeValue to ALLOWED,
            IGNORED.modeValue to IGNORED,
            ERRORED.modeValue to ERRORED,
            DEFAULT.modeValue to DEFAULT
        )

        fun getByModeValue(modeValue: Int): AppOpModes {
            return byMode.get(modeValue) ?: DEFAULT
        }
    }
}
