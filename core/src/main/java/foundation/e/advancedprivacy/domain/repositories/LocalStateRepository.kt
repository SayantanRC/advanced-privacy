/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.domain.repositories

import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.entities.FeatureState
import foundation.e.advancedprivacy.domain.entities.LocationMode
import foundation.e.advancedprivacy.domain.entities.MainFeatures
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow

interface LocalStateRepository {
    val blockTrackers: StateFlow<Boolean>
    fun setBlockTrackers(enabled: Boolean)

    val areAllTrackersBlocked: MutableStateFlow<Boolean>

    val fakeLocationEnabled: StateFlow<Boolean>
    fun setFakeLocationEnabled(enabled: Boolean)

    var fakeLocation: Pair<Float, Float>

    val locationMode: MutableStateFlow<LocationMode>

    fun setIpScramblingSetting(enabled: Boolean)
    val ipScramblingSetting: StateFlow<Boolean>

    val internetPrivacyMode: MutableStateFlow<FeatureState>

    suspend fun emitStartVpnDisclaimer(feature: MainFeatures)

    val startVpnDisclaimer: SharedFlow<MainFeatures>

    suspend fun emitOtherVpnRunning(appDesc: ApplicationDescription)
    val otherVpnRunning: SharedFlow<ApplicationDescription>

    var firstBoot: Boolean

    var hideWarningTrackers: Boolean

    var hideWarningLocation: Boolean

    var hideWarningIpScrambling: Boolean

    var trackersScreenLastPosition: Int
}
