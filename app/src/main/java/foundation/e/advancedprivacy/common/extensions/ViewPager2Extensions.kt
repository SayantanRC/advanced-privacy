/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.common.extensions

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2

fun ViewPager2.findViewHolderForAdapterPosition(position: Int): RecyclerView.ViewHolder? {
    return (getChildAt(0) as RecyclerView).findViewHolderForAdapterPosition(position)
}

fun ViewPager2.updatePagerHeightForChild(itemView: View) {
    itemView.post {
        val wMeasureSpec =
            View.MeasureSpec.makeMeasureSpec(itemView.width, View.MeasureSpec.EXACTLY)
        val hMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        itemView.measure(wMeasureSpec, hMeasureSpec)

        if (layoutParams.height != itemView.measuredHeight) {
            layoutParams = (layoutParams)
                .also { lp ->
                    // applying Fragment Root View Height to
                    // the pager LayoutParams, so they match
                    lp.height = itemView.measuredHeight
                }
        }
    }
}
