/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.common

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.appbar.MaterialToolbar
import foundation.e.advancedprivacy.R

abstract class NavToolbarFragment(@LayoutRes contentLayoutId: Int) : Fragment(contentLayoutId) {

    /**
     * @return title to be used in toolbar
     */
    open fun getTitle(): CharSequence {
        return findNavController().currentDestination?.label ?: ""
    }

    fun setTitle(title: CharSequence?) {
        getToolbar()?.title = title
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<MaterialToolbar>(R.id.toolbar).let { toolbar ->
            toolbar.title = getTitle()
            toolbar.setNavigationOnClickListener {
                onNavigateUp()
            }
        }
    }

    open fun onNavigateUp(): Boolean {
        return findNavController().navigateUp()
    }

    fun getToolbar(): MaterialToolbar? = view?.findViewById(R.id.toolbar)
}
