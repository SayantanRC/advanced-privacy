/*
 * Copyright (C) 2022 - 2023 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy

import android.app.Application
import foundation.e.advancedprivacy.common.WarningDialog
import foundation.e.advancedprivacy.domain.usecases.FakeLocationStateUseCase
import foundation.e.advancedprivacy.domain.usecases.GetQuickPrivacyStateUseCase
import foundation.e.advancedprivacy.domain.usecases.IpScramblingStateUseCase
import foundation.e.advancedprivacy.domain.usecases.ShowFeaturesWarningUseCase
import foundation.e.advancedprivacy.domain.usecases.TrackersStateUseCase
import foundation.e.advancedprivacy.domain.usecases.TrackersStatisticsUseCase
import foundation.e.advancedprivacy.domain.usecases.VpnSupervisorUseCase
import foundation.e.advancedprivacy.externalinterfaces.permissions.IPermissionsPrivacyModule
import foundation.e.advancedprivacy.trackers.services.UpdateTrackersWorker
import foundation.e.lib.telemetry.Telemetry
import kotlinx.coroutines.CoroutineScope
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.java.KoinJavaComponent.get

class AdvancedPrivacyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Telemetry.init(BuildConfig.SENTRY_DSN, this, true)

        startKoin {
            androidContext(this@AdvancedPrivacyApplication)
            modules(appModule)
        }
        initBackgroundSingletons()
    }

    private fun initBackgroundSingletons() {
        UpdateTrackersWorker.periodicUpdate(this)

        WarningDialog.startListening(
            get(ShowFeaturesWarningUseCase::class.java),
            get(CoroutineScope::class.java),
            this
        )

        Widget.startListening(
            this,
            get(GetQuickPrivacyStateUseCase::class.java),
            get(TrackersStatisticsUseCase::class.java)
        )

        Notifications.startListening(
            this,
            get(GetQuickPrivacyStateUseCase::class.java),
            get(IPermissionsPrivacyModule::class.java),
            get(CoroutineScope::class.java)
        )

        get<IpScramblingStateUseCase>(IpScramblingStateUseCase::class.java)
        get<TrackersStateUseCase>(TrackersStateUseCase::class.java)
        get<FakeLocationStateUseCase>(FakeLocationStateUseCase::class.java)
        get<VpnSupervisorUseCase>(VpnSupervisorUseCase::class.java).listenSettings()
    }
}
