/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.data.repositories

import android.content.Context
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.entities.FeatureState
import foundation.e.advancedprivacy.domain.entities.LocationMode
import foundation.e.advancedprivacy.domain.entities.MainFeatures
import foundation.e.advancedprivacy.domain.repositories.LocalStateRepository
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class LocalStateRepositoryImpl(context: Context) : LocalStateRepository {
    companion object {
        private const val SHARED_PREFS_FILE = "localState"
        private const val KEY_BLOCK_TRACKERS = "blockTrackers"
        private const val KEY_IP_SCRAMBLING = "ipScrambling"
        private const val KEY_FAKE_LOCATION = "fakeLocation"
        private const val KEY_FAKE_LATITUDE = "fakeLatitude"
        private const val KEY_FAKE_LONGITUDE = "fakeLongitude"
        private const val KEY_FIRST_BOOT = "firstBoot"
        private const val KEY_HIDE_WARNING_TRACKERS = "hide_warning_trackers"
        private const val KEY_HIDE_WARNING_LOCATION = "hide_warning_location"
        private const val KEY_HIDE_WARNING_IPSCRAMBLING = "hide_warning_ipscrambling"
        private const val KEY_TRACKERS_SCREEN_LAST_POSITION = "trackers_screen_last_position"
    }

    private val sharedPref = context.getSharedPreferences(SHARED_PREFS_FILE, Context.MODE_PRIVATE)

    private val _blockTrackers = MutableStateFlow(sharedPref.getBoolean(KEY_BLOCK_TRACKERS, true))

    override val blockTrackers = _blockTrackers.asStateFlow()

    override fun setBlockTrackers(enabled: Boolean) {
        set(KEY_BLOCK_TRACKERS, enabled)
        _blockTrackers.update { enabled }
    }

    override val areAllTrackersBlocked: MutableStateFlow<Boolean> = MutableStateFlow(false)

    private val _fakeLocationEnabled = MutableStateFlow(sharedPref.getBoolean(KEY_FAKE_LOCATION, false))

    override val fakeLocationEnabled = _fakeLocationEnabled.asStateFlow()

    override fun setFakeLocationEnabled(enabled: Boolean) {
        set(KEY_FAKE_LOCATION, enabled)
        _fakeLocationEnabled.update { enabled }
    }

    override var fakeLocation: Pair<Float, Float>
        get() = Pair(
            // Initial default value is Quezon City
            sharedPref.getFloat(KEY_FAKE_LATITUDE, 14.6760f),
            sharedPref.getFloat(KEY_FAKE_LONGITUDE, 121.0437f)
        )

        set(value) {
            sharedPref.edit()
                .putFloat(KEY_FAKE_LATITUDE, value.first)
                .putFloat(KEY_FAKE_LONGITUDE, value.second)
                .apply()
        }

    override val locationMode: MutableStateFlow<LocationMode> = MutableStateFlow(LocationMode.REAL_LOCATION)

    private val _ipScramblingSetting = MutableStateFlow(sharedPref.getBoolean(KEY_IP_SCRAMBLING, false))

    override val ipScramblingSetting = _ipScramblingSetting.asStateFlow()

    override fun setIpScramblingSetting(enabled: Boolean) {
        set(KEY_IP_SCRAMBLING, enabled)
        _ipScramblingSetting.update { enabled }
    }

    override val internetPrivacyMode: MutableStateFlow<FeatureState> = MutableStateFlow(FeatureState.OFF)

    private val _startVpnDisclaimer = MutableSharedFlow<MainFeatures>()

    override suspend fun emitStartVpnDisclaimer(feature: MainFeatures) {
        _startVpnDisclaimer.emit(feature)
    }

    override val startVpnDisclaimer: SharedFlow<MainFeatures> = _startVpnDisclaimer

    private val _otherVpnRunning = MutableSharedFlow<ApplicationDescription>()

    override suspend fun emitOtherVpnRunning(appDesc: ApplicationDescription) {
        _otherVpnRunning.emit(appDesc)
    }

    override val otherVpnRunning: SharedFlow<ApplicationDescription> = _otherVpnRunning

    override var firstBoot: Boolean
        get() = sharedPref.getBoolean(KEY_FIRST_BOOT, true)
        set(value) = set(KEY_FIRST_BOOT, value)

    override var hideWarningTrackers: Boolean
        get() = sharedPref.getBoolean(KEY_HIDE_WARNING_TRACKERS, false)
        set(value) = set(KEY_HIDE_WARNING_TRACKERS, value)

    override var hideWarningLocation: Boolean
        get() = sharedPref.getBoolean(KEY_HIDE_WARNING_LOCATION, false)
        set(value) = set(KEY_HIDE_WARNING_LOCATION, value)

    override var hideWarningIpScrambling: Boolean
        get() = sharedPref.getBoolean(KEY_HIDE_WARNING_IPSCRAMBLING, false)
        set(value) = set(KEY_HIDE_WARNING_IPSCRAMBLING, value)

    override var trackersScreenLastPosition: Int
        get() = sharedPref.getInt(KEY_TRACKERS_SCREEN_LAST_POSITION, 0)
        set(value) = sharedPref.edit().putInt(KEY_TRACKERS_SCREEN_LAST_POSITION, value).apply()

    private fun set(key: String, value: Boolean) {
        sharedPref.edit().putBoolean(key, value).apply()
    }
}
