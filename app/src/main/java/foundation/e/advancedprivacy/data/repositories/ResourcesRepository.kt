/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.data.repositories

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import androidx.annotation.StringRes
import java.time.format.DateTimeFormatter
import java.util.Locale
import timber.log.Timber

class ResourcesRepository(private val context: Context) {
    private val defaultResources by lazy { getLocalizedResources(context, Locale("")) }

    private fun getLocalizedResources(context: Context, desiredLocale: Locale?): Resources {
        var conf: Configuration = context.resources.configuration
        conf = Configuration(conf)
        conf.setLocale(desiredLocale)
        val localizedContext = context.createConfigurationContext(conf)
        return localizedContext.resources
    }

    fun getFormatter(@StringRes formatRes: Int): DateTimeFormatter {
        return runCatching {
            DateTimeFormatter.ofPattern(context.getString(formatRes))
        }.getOrElse {
            Timber.w(it, "Can't parse DateTimeFormatter")
            DateTimeFormatter.ofPattern(defaultResources.getString(formatRes))
        }
    }
}
