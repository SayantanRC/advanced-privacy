/*
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.domain.usecases

import foundation.e.advancedprivacy.data.repositories.AppListsRepository
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import kotlinx.coroutines.flow.Flow

class AppListUseCase(
    private val appListsRepository: AppListsRepository
) {
    val dummySystemApp = appListsRepository.dummySystemApp
    fun getApp(uid: Int): ApplicationDescription {
        return when (uid) {
            dummySystemApp.uid -> dummySystemApp
            appListsRepository.dummyCompatibilityApp.uid ->
                appListsRepository.dummyCompatibilityApp
            else -> appListsRepository.getApp(uid) ?: dummySystemApp
        }
    }
    fun getAppsUsingInternet(): Flow<List<ApplicationDescription>> {
        return appListsRepository.mainProfileApps()
    }
}
