/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.domain.usecases

import foundation.e.advancedprivacy.data.repositories.AppListsRepository
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.trackers.data.StatsDatabase
import foundation.e.advancedprivacy.trackers.data.TrackersRepository
import foundation.e.advancedprivacy.trackers.data.WhitelistRepository
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker
import foundation.e.advancedprivacy.trackers.domain.usecases.FilterHostnameUseCase

class AppTrackersUseCase(
    private val whitelistRepository: WhitelistRepository,
    private val trackersStateUseCase: TrackersStateUseCase,
    private val appListsRepository: AppListsRepository,
    private val statsDatabase: StatsDatabase,
    private val trackersRepository: TrackersRepository,
    private val filterHostnameUseCase: FilterHostnameUseCase
) {
    suspend fun toggleAppWhitelist(app: ApplicationDescription, trackers: List<Tracker>, isBlocked: Boolean) {
        val realApIds = appListsRepository.getRealApps(app).map { it.apId }
        val trackerIds = trackers.map { it.id }

        whitelistRepository.setWhiteListed(realApIds, !isBlocked)
        whitelistRepository.setWhitelistedTrackersForApps(realApIds, trackerIds, !isBlocked)
        trackersStateUseCase.updateAllTrackersBlockedState()
    }

    suspend fun clearWhitelist(app: ApplicationDescription) {
        appListsRepository.applyForHiddenApps(
            app
        ) {
            whitelistRepository.clearWhiteList(it.apId)
        }
        trackersStateUseCase.updateAllTrackersBlockedState()
    }

    suspend fun getCalls(app: ApplicationDescription): Pair<Int, Int> {
        return appListsRepository.mapReduceForHiddenApps(
            app = app,
            map = {
                statsDatabase.getCallsForApp(app.apId)
            },
            reduce = { zip ->
                zip.unzip().let { (blocked, leaked) ->
                    blocked.sum() to leaked.sum()
                }
            }
        )
    }

    suspend fun getTrackersWithBlockedList(app: ApplicationDescription): List<Pair<Tracker, Boolean>> {
        val realApIds = appListsRepository.getRealApps(app).map { it.apId }
        val trackers = statsDatabase.getTrackerIds(realApIds)
            .mapNotNull { trackersRepository.getTracker(it) }

        return enrichWithBlockedState(app, trackers)
    }

    suspend fun enrichWithBlockedState(app: ApplicationDescription, trackers: List<Tracker>): List<Pair<Tracker, Boolean>> {
        val realAppUids = appListsRepository.getRealApps(app).map { it.uid }
        return trackers.map { tracker ->
            tracker to !realAppUids.any { uid ->
                filterHostnameUseCase.isWhitelisted(uid, tracker.id)
            }
        }.sortedBy { it.first.label.lowercase() }
    }
}
