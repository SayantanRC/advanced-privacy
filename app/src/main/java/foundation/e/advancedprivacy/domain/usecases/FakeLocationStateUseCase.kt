/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.domain.usecases

import android.app.AppOpsManager
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import foundation.e.advancedprivacy.domain.entities.AppOpModes
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.entities.LocationMode
import foundation.e.advancedprivacy.domain.repositories.LocalStateRepository
import foundation.e.advancedprivacy.dummy.CityDataSource
import foundation.e.advancedprivacy.externalinterfaces.permissions.IPermissionsPrivacyModule
import foundation.e.advancedprivacy.fakelocation.domain.usecases.FakeLocationModule
import kotlin.random.Random
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import timber.log.Timber

class FakeLocationStateUseCase(
    private val fakeLocationModule: FakeLocationModule,
    private val permissionsModule: IPermissionsPrivacyModule,
    private val localStateRepository: LocalStateRepository,
    private val citiesRepository: CityDataSource,
    private val appDesc: ApplicationDescription,
    private val appContext: Context,
    coroutineScope: CoroutineScope
) {
    private val _configuredLocationMode = MutableStateFlow<Triple<LocationMode, Float?, Float?>>(
        Triple(LocationMode.REAL_LOCATION, null, null)
    )

    val configuredLocationMode: StateFlow<Triple<LocationMode, Float?, Float?>> = _configuredLocationMode

    init {
        coroutineScope.launch {
            localStateRepository.fakeLocationEnabled.collect {
                applySettings(it, localStateRepository.fakeLocation)
            }
        }
    }

    private val locationManager: LocationManager
        get() = appContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    private fun hasAcquireLocationPermission(): Boolean {
        return (appContext.checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) ||
            permissionsModule.toggleDangerousPermission(appDesc, android.Manifest.permission.ACCESS_FINE_LOCATION, true)
    }

    private fun applySettings(isEnabled: Boolean, fakeLocation: Pair<Float, Float>, isSpecificLocation: Boolean = false) {
        _configuredLocationMode.value = computeLocationMode(isEnabled, fakeLocation, isSpecificLocation)

        if (isEnabled && hasAcquireMockLocationPermission()) {
            fakeLocationModule.startFakeLocation()
            fakeLocationModule.setFakeLocation(fakeLocation.first.toDouble(), fakeLocation.second.toDouble())
            localStateRepository.locationMode.value = configuredLocationMode.value.first
        } else {
            fakeLocationModule.stopFakeLocation()
            localStateRepository.locationMode.value = LocationMode.REAL_LOCATION
        }
    }

    private fun hasAcquireMockLocationPermission(): Boolean {
        return (permissionsModule.getAppOpMode(appDesc, AppOpsManager.OPSTR_MOCK_LOCATION) == AppOpModes.ALLOWED) ||
            permissionsModule.setAppOpMode(appDesc, AppOpsManager.OPSTR_MOCK_LOCATION, AppOpModes.ALLOWED)
    }

    fun setSpecificLocation(latitude: Float, longitude: Float) {
        setFakeLocation(latitude to longitude, true)
    }

    fun setRandomLocation() {
        val randomIndex = Random.nextInt(citiesRepository.citiesLocationsList.size)
        val location = citiesRepository.citiesLocationsList[randomIndex]

        setFakeLocation(location)
    }

    private fun setFakeLocation(location: Pair<Float, Float>, isSpecificLocation: Boolean = false) {
        localStateRepository.fakeLocation = location
        localStateRepository.setFakeLocationEnabled(true)
        applySettings(true, location, isSpecificLocation)
    }

    fun stopFakeLocation() {
        localStateRepository.setFakeLocationEnabled(false)
        applySettings(false, localStateRepository.fakeLocation)
    }

    private fun computeLocationMode(
        isFakeLocationEnabled: Boolean,
        fakeLocation: Pair<Float, Float>,
        isSpecificLocation: Boolean = false
    ): Triple<LocationMode, Float?, Float?> {
        return Triple(
            when {
                !isFakeLocationEnabled -> LocationMode.REAL_LOCATION
                (fakeLocation in citiesRepository.citiesLocationsList && !isSpecificLocation) ->
                    LocationMode.RANDOM_LOCATION
                else -> LocationMode.SPECIFIC_LOCATION
            },
            fakeLocation.first,
            fakeLocation.second
        )
    }

    val currentLocation = MutableStateFlow<Location?>(null)

    private var localListener = object : LocationListener {

        override fun onLocationChanged(location: Location) {
            currentLocation.update { location }
        }

        @Deprecated("Deprecated since API 29, never called.")
        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

        override fun onProviderEnabled(provider: String) {
            reset()
        }

        override fun onProviderDisabled(provider: String) {
            reset()
        }

        private fun reset() {
            stopListeningLocation()
            currentLocation.value = null
            startListeningLocation()
        }
    }

    fun startListeningLocation(): Boolean {
        return if (hasAcquireLocationPermission()) {
            requestLocationUpdates()
            true
        } else {
            false
        }
    }

    fun stopListeningLocation() {
        locationManager.removeUpdates(localListener)
    }

    private fun requestLocationUpdates() {
        val networkProvider = LocationManager.NETWORK_PROVIDER
            .takeIf { it in locationManager.allProviders }
        val gpsProvider = LocationManager.GPS_PROVIDER
            .takeIf { it in locationManager.allProviders }

        try {
            networkProvider?.let {
                locationManager.requestLocationUpdates(
                    it,
                    1000L,
                    0f,
                    localListener
                )
            }
            gpsProvider?.let {
                locationManager.requestLocationUpdates(
                    it,
                    1000L,
                    0f,
                    localListener
                )
            }

            var lastKnownLocation = networkProvider?.let {
                locationManager.getLastKnownLocation(it)
            }

            if (lastKnownLocation == null) {
                lastKnownLocation = gpsProvider?.let {
                    locationManager.getLastKnownLocation(it)
                }
            }

            lastKnownLocation?.let { localListener.onLocationChanged(it) }
        } catch (se: SecurityException) {
            Timber.e(se, "Missing permission")
        }
    }
}
