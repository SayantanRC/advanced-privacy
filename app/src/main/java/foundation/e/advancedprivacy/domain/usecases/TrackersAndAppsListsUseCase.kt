/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.domain.usecases

import foundation.e.advancedprivacy.data.repositories.AppListsRepository
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.entities.TrackersAndAppsLists
import foundation.e.advancedprivacy.features.trackers.AppWithTrackersCount
import foundation.e.advancedprivacy.features.trackers.Period
import foundation.e.advancedprivacy.features.trackers.TrackerWithAppsCount
import foundation.e.advancedprivacy.trackers.data.StatsDatabase
import foundation.e.advancedprivacy.trackers.data.TrackersRepository
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker
import java.time.Instant
import kotlinx.coroutines.flow.first

class TrackersAndAppsListsUseCase(
    private val statsDatabase: StatsDatabase,
    private val trackersRepository: TrackersRepository,
    private val appListsRepository: AppListsRepository
) {
    suspend fun getAppsAndTrackersCounts(period: Period): TrackersAndAppsLists {
        val periodStart: Instant = period.getPeriodStart()
        val trackersAndAppsIds = statsDatabase.getDistinctTrackerAndApp(periodStart)
        val trackersAndApps = mapIdsToEntities(trackersAndAppsIds)
        val (countByApp, countByTracker) = foldToCountByEntityMaps(trackersAndApps)

        return TrackersAndAppsLists(
            trackers = buildTrackerList(countByTracker),
            allApps = buildAllAppList(countByApp),
            appsWithTrackers = buildAppList(countByApp)
        )
    }

    private fun buildTrackerList(countByTracker: Map<Tracker, Int>): List<TrackerWithAppsCount> {
        return countByTracker.map { (tracker, count) ->
            TrackerWithAppsCount(tracker = tracker, appsCount = count)
        }.sortedByDescending { it.appsCount }
    }

    private suspend fun buildAllAppList(countByApp: Map<ApplicationDescription, Int>): List<AppWithTrackersCount> {
        return appListsRepository.apps().first().map { app: ApplicationDescription ->
            AppWithTrackersCount(app = app, trackersCount = countByApp[app] ?: 0)
        }.sortedByDescending { it.trackersCount }
    }

    private fun buildAppList(countByApp: Map<ApplicationDescription, Int>): List<AppWithTrackersCount> {
        return countByApp.map { (app, count) ->
            AppWithTrackersCount(app = app, trackersCount = count)
        }.sortedByDescending { it.trackersCount }
    }

    private suspend fun mapIdsToEntities(trackersAndAppsIds: List<Pair<String, String>>): List<Pair<Tracker, ApplicationDescription>> {
        return trackersAndAppsIds.mapNotNull { (trackerId, apId) ->
            trackersRepository.getTracker(trackerId)?.let { tracker ->
                appListsRepository.getDisplayableApp(apId)?.let { app ->
                    tracker to app
                }
            }
            // appListsRepository.getDisplayableApp() may transform many apId to one
            // ApplicationDescription, so the lists is not distinct anymore.
        }.distinct()
    }

    private fun foldToCountByEntityMaps(
        trackersAndApps: List<Pair<Tracker, ApplicationDescription>>
    ): Pair<Map<ApplicationDescription, Int>, Map<Tracker, Int>> {
        return trackersAndApps.fold(
            mutableMapOf<ApplicationDescription, Int>() to mutableMapOf<Tracker, Int>()
        ) { (countByApp, countByTracker), (tracker, app) ->
            countByApp[app] = countByApp.getOrDefault(app, 0) + 1
            countByTracker[tracker] = countByTracker.getOrDefault(tracker, 0) + 1
            countByApp to countByTracker
        }
    }
}
