/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.domain.usecases

import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.entities.FeatureState
import foundation.e.advancedprivacy.domain.entities.LocationMode
import foundation.e.advancedprivacy.domain.entities.TrackerMode
import foundation.e.advancedprivacy.domain.repositories.LocalStateRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map

class GetQuickPrivacyStateUseCase(
    private val localStateRepository: LocalStateRepository
) {
    val trackerMode: Flow<TrackerMode> = combine(
        localStateRepository.blockTrackers,
        localStateRepository.areAllTrackersBlocked
    ) { isBlockTrackers, isAllTrackersBlocked ->
        when {
            isBlockTrackers && isAllTrackersBlocked -> TrackerMode.DENIED
            isBlockTrackers && !isAllTrackersBlocked -> TrackerMode.CUSTOM
            else -> TrackerMode.VULNERABLE
        }
    }

    val isLocationHidden: Flow<Boolean> = localStateRepository.locationMode.map { locationMode ->
        locationMode != LocationMode.REAL_LOCATION
    }

    val ipScramblingMode: Flow<FeatureState> =
        localStateRepository.internetPrivacyMode

    fun toggleTrackers(enabled: Boolean?) {
        val value = enabled ?: !localStateRepository.blockTrackers.value
        if (value != localStateRepository.blockTrackers.value) {
            localStateRepository.setBlockTrackers(value)
        }
    }

    fun toggleLocation(enabled: Boolean?) {
        val value = enabled ?: !localStateRepository.fakeLocationEnabled.value
        if (value != localStateRepository.fakeLocationEnabled.value) {
            localStateRepository.setFakeLocationEnabled(value)
        }
    }

    fun toggleIpScrambling(enabled: Boolean?) {
        val value = enabled ?: !localStateRepository.ipScramblingSetting.value
        if (value != localStateRepository.ipScramblingSetting.value) {
            localStateRepository.setIpScramblingSetting(value)
        }
    }

    val otherVpnRunning: SharedFlow<ApplicationDescription> = localStateRepository.otherVpnRunning
}
