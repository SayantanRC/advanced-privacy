/*
 * Copyright (C) 2022 - 2023 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.domain.usecases

import foundation.e.advancedprivacy.data.repositories.AppListsRepository
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.repositories.LocalStateRepository
import foundation.e.advancedprivacy.trackers.data.WhitelistRepository
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class TrackersStateUseCase(
    private val whitelistRepository: WhitelistRepository,
    private val localStateRepository: LocalStateRepository,
    private val appListsRepository: AppListsRepository,
    coroutineScope: CoroutineScope
) {
    init {
        coroutineScope.launch {
            localStateRepository.blockTrackers.collect { enabled ->
                whitelistRepository.isBlockingEnabled = enabled
                updateAllTrackersBlockedState()
            }
        }
    }

    fun updateAllTrackersBlockedState() {
        localStateRepository.areAllTrackersBlocked.value = whitelistRepository.isBlockingEnabled &&
            whitelistRepository.areWhiteListEmpty()
    }

    fun isWhitelisted(app: ApplicationDescription): Boolean {
        return isWhitelisted(app, appListsRepository, whitelistRepository)
    }

    fun isWhitelisted(tracker: Tracker): Boolean {
        return whitelistRepository.isWhiteListed(tracker)
    }

    suspend fun blockTracker(app: ApplicationDescription, tracker: Tracker, isBlocked: Boolean) {
        whitelistRepository.setWhitelistedAppsForTracker(
            appListsRepository.getRealApps(app).map { it.apId },
            tracker.id,
            !isBlocked
        )
        updateAllTrackersBlockedState()
    }
}

fun isWhitelisted(app: ApplicationDescription, appListsRepository: AppListsRepository, whitelistRepository: WhitelistRepository): Boolean {
    return appListsRepository.anyForHiddenApps(
        app,
        whitelistRepository::isAppWhiteListed
    )
}
