/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.domain.usecases

import foundation.e.advancedprivacy.data.repositories.AppListsRepository
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.trackers.data.StatsDatabase
import foundation.e.advancedprivacy.trackers.data.WhitelistRepository
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker
import foundation.e.advancedprivacy.trackers.domain.usecases.FilterHostnameUseCase

class TrackerDetailsUseCase(
    private val whitelistRepository: WhitelistRepository,
    private val trackersStateUseCase: TrackersStateUseCase,
    private val appListsRepository: AppListsRepository,
    private val statsDatabase: StatsDatabase,
    private val filterHostnameUseCase: FilterHostnameUseCase
) {
    suspend fun toggleTrackerWhitelist(tracker: Tracker, apps: List<ApplicationDescription>, isBlocked: Boolean) {
        whitelistRepository.setWhiteListed(tracker, !isBlocked)
        whitelistRepository.setWhitelistedAppsForTracker(
            apps.flatMap { appListsRepository.getRealApps(it) }.map { it.apId },
            tracker.id,
            !isBlocked
        )
        trackersStateUseCase.updateAllTrackersBlockedState()
    }

    suspend fun getAppsWithBlockedState(tracker: Tracker): List<Pair<ApplicationDescription, Boolean>> {
        return enrichWithBlockedState(
            statsDatabase.getApIds(tracker.id).mapNotNull {
                appListsRepository.getDisplayableApp(it)
            }.sortedBy { it.label?.toString() },
            tracker
        )
    }

    suspend fun enrichWithBlockedState(apps: List<ApplicationDescription>, tracker: Tracker): List<Pair<ApplicationDescription, Boolean>> {
        return apps.map { app ->
            app to appListsRepository.anyForHiddenApps(app) { realApp ->
                !filterHostnameUseCase.isWhitelisted(realApp.uid, tracker.id)
            }
        }
    }

    suspend fun getCalls(tracker: Tracker): Pair<Int, Int> {
        return statsDatabase.getCallsForTracker(tracker.id)
    }
}
