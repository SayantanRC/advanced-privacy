/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.domain.usecases

import foundation.e.advancedprivacy.data.repositories.AppListsRepository
import foundation.e.advancedprivacy.domain.entities.FeatureState
import foundation.e.advancedprivacy.domain.repositories.LocalStateRepository
import foundation.e.advancedprivacy.ipscrambler.OrbotSupervisor
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map

class IpScramblingStateUseCase(
    private val orbotSupervisor: OrbotSupervisor,
    private val localStateRepository: LocalStateRepository,
    private val appListsRepository: AppListsRepository,
    private val coroutineScope: CoroutineScope
) {
    val internetPrivacyMode: StateFlow<FeatureState> = orbotSupervisor.state

    init {
        orbotSupervisor.requestStatus()

        orbotSupervisor.state.map {
            localStateRepository.internetPrivacyMode.value = it
        }.launchIn(coroutineScope)
    }

    fun toggle(hideIp: Boolean) {
        localStateRepository.setIpScramblingSetting(enabled = hideIp)
    }

    private fun getHiddenPackageNames(): List<String> {
        return appListsRepository.getMainProfileHiddenSystemApps().map { it.packageName }
    }

    val bypassTorApps: Set<String> get() {
        var whitelist = orbotSupervisor.appList
        if (getHiddenPackageNames().any { it in whitelist }) {
            val mutable = whitelist.toMutableSet()
            mutable.removeAll(getHiddenPackageNames())
            mutable.add(appListsRepository.dummySystemApp.packageName)
            whitelist = mutable
        }
        if (AppListsRepository.compatibiltyPNames.any { it in whitelist }) {
            val mutable = whitelist.toMutableSet()
            mutable.removeAll(AppListsRepository.compatibiltyPNames)
            mutable.add(appListsRepository.dummyCompatibilityApp.packageName)
            whitelist = mutable
        }
        return whitelist
    }

    fun toggleBypassTor(packageName: String) {
        val visibleList = bypassTorApps.toMutableSet()
        val rawList = orbotSupervisor.appList.toMutableSet()

        if (visibleList.contains(packageName)) {
            if (packageName == appListsRepository.dummySystemApp.packageName) {
                rawList.removeAll(getHiddenPackageNames())
            } else if (packageName == appListsRepository.dummyCompatibilityApp.packageName) {
                rawList.removeAll(AppListsRepository.compatibiltyPNames)
            } else {
                rawList.remove(packageName)
            }
        } else {
            if (packageName == appListsRepository.dummySystemApp.packageName) {
                rawList.addAll(getHiddenPackageNames())
            } else if (packageName == appListsRepository.dummyCompatibilityApp.packageName) {
                rawList.addAll(AppListsRepository.compatibiltyPNames)
            } else {
                rawList.add(packageName)
            }
        }
        orbotSupervisor.appList = rawList
    }

    val availablesLocations: List<String> = orbotSupervisor.getAvailablesLocations().sorted()

    val exitCountry: String get() = orbotSupervisor.getExitCountryCode()

    suspend fun setExitCountry(locationId: String) {
        if (locationId != exitCountry) {
            orbotSupervisor.setExitCountryCode(locationId)
        }
    }
}
