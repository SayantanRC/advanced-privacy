/*
 * Copyright (C) 2022 - 2023 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.databinding.TrackersItemAppBinding

class AppsAdapter(
    private val viewModel: TrackersPeriodViewModel
) :
    RecyclerView.Adapter<AppsAdapter.ViewHolder>() {

    class ViewHolder(view: View, private val parentViewModel: TrackersPeriodViewModel) : RecyclerView.ViewHolder(view) {
        val binding = TrackersItemAppBinding.bind(view)
        fun bind(item: AppWithTrackersCount) {
            binding.icon.setImageDrawable(item.app.icon)
            binding.title.text = item.app.label
            binding.counts.text = itemView.context.getString(R.string.trackers_list_app_trackers_counts, item.trackersCount.toString())
            itemView.setOnClickListener {
                parentViewModel.onClickApp(item.app)
            }
        }
    }

    var dataSet: List<AppWithTrackersCount> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.trackers_item_app, parent, false)
        return ViewHolder(view, viewModel)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val app = dataSet[position]
        holder.bind(app)
    }

    override fun getItemCount(): Int = dataSet.size
}
