/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.dashboard

import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.domain.usecases.GetQuickPrivacyStateUseCase
import foundation.e.advancedprivacy.domain.usecases.TrackersStatisticsUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DashboardViewModel(
    private val getPrivacyStateUseCase: GetQuickPrivacyStateUseCase,
    private val trackersStatisticsUseCase: TrackersStatisticsUseCase
) : ViewModel() {

    private val _state = MutableStateFlow(DashboardState())
    val state = _state.asStateFlow()

    private val _singleEvents = MutableSharedFlow<SingleEvent>()
    val singleEvents = _singleEvents.asSharedFlow()

    private val _navigate = MutableSharedFlow<NavDirections>()
    val navigate = _navigate.asSharedFlow()
    init {
        viewModelScope.launch(Dispatchers.IO) { trackersStatisticsUseCase.initAppList() }
    }

    suspend fun doOnStartedState() = withContext(Dispatchers.IO) {
        merge(
            getPrivacyStateUseCase.ipScramblingMode.map {
                _state.update { s -> s.copy(ipScramblingMode = it) }
            },

            trackersStatisticsUseCase.listenUpdates().mapLatest {
                fetchStatistics()
            },

            getPrivacyStateUseCase.trackerMode.map {
                _state.update { s -> s.copy(trackerMode = it) }
            },
            getPrivacyStateUseCase.isLocationHidden.map {
                _state.update { s -> s.copy(isLocationHidden = it) }
            },
            getPrivacyStateUseCase.otherVpnRunning.map {
                _singleEvents.emit(
                    SingleEvent.ToastMessageSingleEvent(
                        R.string.ipscrambling_error_always_on_vpn_already_running,
                        listOf(it.label ?: "")
                    )
                )
            }
        ).collect {}
    }

    fun onClickViewTrackersStatistics() = viewModelScope.launch {
        _navigate.emit(DashboardFragmentDirections.gotoTrackersFragment())
    }

    fun onClickTrackersControl() = viewModelScope.launch {
        _navigate.emit(DashboardFragmentDirections.gotoTrackersFragment())
    }

    fun onClickToggleTrackersContol(enabled: Boolean) = viewModelScope.launch(Dispatchers.IO) {
        getPrivacyStateUseCase.toggleTrackers(enabled)
    }

    fun onClickFakeLocation() = viewModelScope.launch {
        _navigate.emit(DashboardFragmentDirections.gotoFakeLocationFragment())
    }

    fun onClickToggleFakeLocation(enabled: Boolean) = viewModelScope.launch(Dispatchers.IO) {
        getPrivacyStateUseCase.toggleLocation(enabled)
    }

    fun onClickIpScrambling() = viewModelScope.launch {
        _navigate.emit(DashboardFragmentDirections.gotoInternetPrivacyFragment())
    }

    fun onClickToggleIpScrambling(enabled: Boolean) = viewModelScope.launch(Dispatchers.IO) {
        getPrivacyStateUseCase.toggleIpScrambling(enabled)
    }

    fun onClickAppsPermissions() = viewModelScope.launch {
        _navigate.emit(DashboardFragmentDirections.gotoSettingsPermissionsActivity())
    }

    private suspend fun fetchStatistics() = withContext(Dispatchers.IO) {
        val blockedCallsCount = trackersStatisticsUseCase.getLastMonthBlockedLeaksCount()

        val appsWithBlockedLeaksCount = trackersStatisticsUseCase.getLastMonthAppsWithBLockedLeaksCount()

        _state.update {
            it.copy(
                blockedCallsCount = blockedCallsCount,
                appsWithCallsCount = appsWithBlockedLeaksCount
            )
        }
    }

    sealed class SingleEvent {
        data class ToastMessageSingleEvent(
            @StringRes val message: Int,
            val args: List<Any> = emptyList()
        ) : SingleEvent()
    }
}
