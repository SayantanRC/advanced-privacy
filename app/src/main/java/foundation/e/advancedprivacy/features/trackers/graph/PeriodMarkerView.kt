/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers.graph

import android.content.Context
import android.graphics.Canvas
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.DynamicDrawableSpan
import android.text.style.ImageSpan
import android.view.View
import android.widget.TextView
import androidx.core.text.toSpannable
import androidx.core.view.isVisible
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.common.extensions.dpToPxF

class PeriodMarkerView(context: Context) : MarkerView(context, R.layout.chart_tooltip) {
    enum class ArrowPosition { LEFT, CENTER, RIGHT }

    private val arrowMargins = 10.dpToPxF(context)
    private val mOffset2 = MPPointF(0f, 0f)

    private fun getArrowPosition(posX: Float): ArrowPosition {
        val halfWidth = width / 2

        return chartView?.let { chart ->
            if (posX < halfWidth) {
                ArrowPosition.LEFT
            } else if (chart.width - posX < halfWidth) {
                ArrowPosition.RIGHT
            } else {
                ArrowPosition.CENTER
            }
        } ?: ArrowPosition.CENTER
    }

    private fun showArrow(position: ArrowPosition?) {
        val ids = listOf(
            R.id.arrow_bottom_left,
            R.id.arrow_bottom_center,
            R.id.arrow_bottom_right
        )

        val toShow = when (position) {
            ArrowPosition.LEFT -> R.id.arrow_bottom_left
            ArrowPosition.CENTER -> R.id.arrow_bottom_center
            ArrowPosition.RIGHT -> R.id.arrow_bottom_right
            else -> null
        }

        ids.forEach { id ->
            val showIt = id == toShow
            findViewById<View>(id)?.let {
                if (it.isVisible != showIt) {
                    it.isVisible = showIt
                }
            }
        }
    }

    fun setLabel(period: String, blocked: Int, leaked: Int) {
        val span = SpannableStringBuilder(period)
        span.append(" |  ")
        span.setSpan(
            ImageSpan(context, R.drawable.ic_legend_blocked, DynamicDrawableSpan.ALIGN_BASELINE),
            span.length - 1,
            span.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        span.append(" $blocked   ")
        span.setSpan(
            ImageSpan(context, R.drawable.ic_legend_leaked, DynamicDrawableSpan.ALIGN_BASELINE),
            span.length - 1,
            span.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        span.append(" $leaked")
        findViewById<TextView>(R.id.label).text = span.toSpannable()
    }

    override fun refreshContent(e: Entry?, highlight: Highlight?) {
        highlight?.let {
            showArrow(getArrowPosition(highlight.xPx))
        }
        super.refreshContent(e, highlight)
    }

    override fun getOffsetForDrawingAtPoint(posX: Float, posY: Float): MPPointF {
        val x = when (getArrowPosition(posX)) {
            ArrowPosition.LEFT -> -arrowMargins
            ArrowPosition.RIGHT -> -width + arrowMargins
            ArrowPosition.CENTER -> -width.toFloat() / 2
        }

        mOffset2.x = x
        mOffset2.y = -posY

        return mOffset2
    }

    override fun draw(canvas: Canvas?, posX: Float, posY: Float) {
        super.draw(canvas, posX, posY)
    }
}
