/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.databinding.TrackersItemAppBinding

class TrackersAdapter(
    val viewModel: TrackersPeriodViewModel
) :
    RecyclerView.Adapter<TrackersAdapter.ViewHolder>() {

    class ViewHolder(view: View, private val parentViewModel: TrackersPeriodViewModel) : RecyclerView.ViewHolder(view) {
        val binding = TrackersItemAppBinding.bind(view)
        init {
            binding.icon.isVisible = false
        }
        fun bind(item: TrackerWithAppsCount) {
            binding.title.text = item.tracker.label
            binding.counts.text = itemView.context.getString(R.string.trackers_list_tracker_apps_counts, item.appsCount.toString())
            itemView.setOnClickListener {
                parentViewModel.onClickTracker(item.tracker)
            }
        }
    }

    var dataSet: List<TrackerWithAppsCount> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.trackers_item_app, parent, false)
        return ViewHolder(view, viewModel)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val app = dataSet[position]
        holder.bind(app)
    }

    override fun getItemCount(): Int = dataSet.size
}
