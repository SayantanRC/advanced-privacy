/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.features.trackers

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.divider.MaterialDividerItemDecoration
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.databinding.TrackersAppsListBinding
import foundation.e.advancedprivacy.databinding.TrackersListBinding

const val TAB_APPS = 0
private const val TAB_TRACKERS = 1

class ListsTabPagerAdapter(
    private val context: Context,
    private val viewModel: TrackersPeriodViewModel
) : RecyclerView.Adapter<ListsTabPagerAdapter.ListsTabViewHolder>() {
    private var uiState: TrackersPeriodState = TrackersPeriodState()

    fun updateDataSet(state: TrackersPeriodState) {
        uiState = state
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int = position

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListsTabViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            TAB_APPS -> {
                ListsTabViewHolder.AppsListViewHolder(
                    TrackersAppsListBinding.inflate(inflater, parent, false),
                    viewModel
                )
            }
            else -> {
                ListsTabViewHolder.TrackersListViewHolder(
                    TrackersListBinding.inflate(inflater, parent, false),
                    viewModel
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return 2
    }

    override fun onBindViewHolder(holder: ListsTabViewHolder, position: Int) {
        when (position) {
            TAB_APPS -> {
                (holder as ListsTabViewHolder.AppsListViewHolder).onBind(uiState)
            }
            TAB_TRACKERS -> {
                (holder as ListsTabViewHolder.TrackersListViewHolder).onBind(uiState.trackers ?: emptyList())
            }
        }
    }

    sealed class ListsTabViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        protected fun setupRecyclerView(recyclerView: RecyclerView) {
            recyclerView.apply {
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
                addItemDecoration(
                    MaterialDividerItemDecoration(context, LinearLayoutManager.VERTICAL).apply {
                        dividerColor = ContextCompat.getColor(context, R.color.divider)
                        dividerInsetStart = 16.dpToPx()
                        dividerInsetEnd = 16.dpToPx()
                    }
                )
            }
        }

        private fun Int.dpToPx(): Int {
            return (this * Resources.getSystem().displayMetrics.density).toInt()
        }

        class AppsListViewHolder(
            private val binding: TrackersAppsListBinding,
            private val viewModel: TrackersPeriodViewModel
        ) : ListsTabViewHolder(binding.root) {
            init {
                setupRecyclerView(binding.list)
                binding.list.adapter = AppsAdapter(viewModel)
                binding.toggleNoTrackerApps.setOnClickListener { viewModel.onToggleHideNoTrackersApps() }
            }

            fun onBind(uiState: TrackersPeriodState) {
                (binding.list.adapter as AppsAdapter).dataSet = (
                    if (uiState.hideNoTrackersApps) {
                        uiState.appsWithTrackers
                    } else {
                        uiState.allApps
                    }
                    ) ?: emptyList()

                binding.toggleNoTrackerApps.apply {
                    isCloseIconVisible = uiState.hideNoTrackersApps
                    isChecked = uiState.hideNoTrackersApps
                }
            }
        }

        class TrackersListViewHolder(
            private val binding: TrackersListBinding,
            private val viewModel: TrackersPeriodViewModel
        ) : ListsTabViewHolder(binding.root) {
            init {
                setupRecyclerView(binding.list)
                binding.list.adapter = TrackersAdapter(viewModel)
            }

            fun onBind(trackers: List<TrackerWithAppsCount>) {
                (binding.list.adapter as TrackersAdapter).dataSet = trackers
            }
        }
    }
}
