/*
 * Copyright (C) 2022 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package android.os;

import android.annotation.TargetApi;
import android.content.pm.UserInfo;

import androidx.annotation.DeprecatedSinceApi;
import androidx.annotation.RequiresPermission;
import java.util.List;

public class UserManager {

    @TargetApi(29)
    @DeprecatedSinceApi(
            api = 34,
            message = "Check availability in SDK34"
    )
    @RequiresPermission("android.permission.MANAGE_USERS")
    public List<UserInfo> getProfiles(int userHandle) {
        return null;
    }

    @TargetApi(29)
    @DeprecatedSinceApi(
            api = 34,
            message = "Check availability in SDK34"
    )
    @RequiresPermission("android.permission.MANAGE_USERS")
    public boolean isManagedProfile(int userId) {
        return false;
    }
}
